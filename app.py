# Copyright (C) 2021 Taavi Väänänen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import os
import re

import bleach
import flask
import opensearchpy
import yaml
from flask import Flask, request

app = Flask(__name__)

__dir__ = os.path.dirname(__file__)
app.config.update(yaml.safe_load(open(os.path.join(__dir__, "config.yaml"))))

os_client = opensearchpy.OpenSearch(
    hosts=app.config["OPENSEARCH_HOSTS"],
    http_auth=(app.config["OPENSEARCH_USERNAME"], app.config["OPENSEARCH_PASSWORD"]),
)


def _query_adjacent_internal(document, count, direction):
    filter_key = "gt" if direction == "next" else "lt"

    search_result = os_client.search(
        index=document["_index"],
        body={
            "size": count,
            "query": {
                "range": {
                    "@timestamp": {
                        filter_key: document["_source"]["@timestamp"],
                    },
                },
            },
            "sort": [
                {
                    "@timestamp": {
                        "order": "asc" if direction == "next" else "desc",
                    },
                }
            ],
            "post_filter": {
                "term": {
                    "channel.keyword": document["_source"]["channel"],
                }
            },
        },
    )

    if direction == "next":
        return search_result["hits"]["hits"]
    else:
        return reversed(search_result["hits"]["hits"])


def query_adjacent(document, count, direction="none"):
    results = []
    real_count = 2 * count if direction in ("prev", "next") else count

    if direction in ("none", "prev"):
        results.extend(_query_adjacent_internal(document, real_count, "previous"))

    results.append(document)

    if direction in ("none", "next"):
        results.extend(_query_adjacent_internal(document, real_count, "next"))

    return {
        "messages": results,
        "length": len(results),
        "match": document,
    }


@app.template_filter("choose_color")
def choose_color(text):
    result = 0
    for char in text:
        result += ord(char)
    return result % 32


@app.template_filter("format_date")
def format_date(text):
    return text[:10]


@app.template_filter("format_timestamp")
def format_timestamp(text):
    return text[11:19]


@app.template_filter("format_log")
def format_log(text):
    text = bleach.clean(text)
    text = bleach.linkify(text)
    text = re.sub(
        r"(^|[^/%])\b([DMTP]\d+)\b",
        '\\1<a href="https://phabricator.wikimedia.org/\\2">\\2</a>',
        text,
    )
    return text


@app.get("/")
def index():
    return flask.render_template("index.html")


@app.get("/search")
def search():
    if "query" not in request.args:
        return flask.redirect("/")

    query_string = request.args.get("query")
    if len(query_string) == 0:
        return flask.redirect("/")

    page = request.args.get("page", "1")
    try:
        page = int(page)
    except ValueError:
        pass
    if page <= 0:
        page = 1

    results = os_client.search(
        index="irc-*",
        body={
            "from": (page - 1) * 25,
            "size": 25 + 1,
            "query": {
                "bool": {
                    "must": {
                        "query_string": {
                            "query": query_string,
                            "default_field": "message",
                            "default_operator": "AND",
                        }
                    },
                    "must_not": {
                        "term": {
                            "nick": "wikibugs",
                        },
                    },
                },
            },
            "sort": [
                {
                    "@timestamp": {
                        "order": "desc",
                    },
                }
            ],
        },
    )

    hits = results["hits"]["hits"]
    has_more_results = False
    if len(hits) == 26:
        hits = hits[:25]
        has_more_results = True

    full_results = [query_adjacent(document, 5) for document in hits]

    return flask.render_template(
        "results.html",
        query=query_string,
        results=full_results,
        page=page,
        show_context_link=True,
        highlight=True,
        has_more_results=has_more_results,
    )


@app.get("/view/<document_index>/<document_id>")
def view(document_index, document_id):
    if not document_index.startswith("irc-"):
        return "Nope", 400

    try:
        result = os_client.get(
            index=document_index,
            id=document_id,
        )
    except opensearchpy.exceptions.NotFoundError:
        return "Not found", 404

    full_results = query_adjacent(result, 50, request.args.get("direction", "none"))
    return flask.render_template(
        "view.html",
        results=[full_results],
        show_next_previous_links=True,
        highlight=request.args.get("direction", "none") == "none",
    )


if __name__ == "__main__":
    app.run()
